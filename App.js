import React from 'react';

import {
  StyleSheet,
  View,
  Platform
} from 'react-native';

import { Scene, Stack, Router } from 'react-native-router-flux';

import HomeView from './src/home.view';
import ArtistDetailView from './src/artist.detail.view';
import LoginView from './src/login.view';

export default class App extends React.Component {
  render() {
    const isAndroid = Platform.OS === 'android';

    return (
      <Router>
        <Stack key="root">
          <Scene key="login" component={LoginView} hideNavBar={false} />
          <Scene key="home" component={HomeView} hideNavBar={true} />
          <Scene key="artistDetail" component={ArtistDetailView} />
        </Stack>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 10,
  },
});
