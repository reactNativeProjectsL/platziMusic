import React from 'react';

import {
  StyleSheet,
  View
} from 'react-native';

import ArtistList from './artistList.component';
import { getArtists } from './api/api-client';

export default class HomeView extends React.Component {
  state = {
    artists: []
  }

  componentDidMount() {
    getArtists()
      .then(data => this.setState({artists: data}));
  }

  render() {
    return (
      <View style={styles.container}>
        <ArtistList artists={this.state.artists} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 10,
  },
});
