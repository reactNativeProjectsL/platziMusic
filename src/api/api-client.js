
const URL = 'http://ws.audioscrobbler.com/2.0/?method=geo.gettopartists&country=colombia&api_key=855de4e5b371940089d46cfcb917bbe4&format=json';
function getArtists() {
  return fetch(URL)
    .then(response => response.json())
    .then(data => data.topartists.artist)
    .then(
      artists => artists.map(artist => {
        return {
          name: artist.name,
          image: artist.image[3]['#text'],
          numLikes: 15,
          numComments: 20
        }
      }))
}

export { getArtists }
