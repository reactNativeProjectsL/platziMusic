import React from 'react';

import {
  StyleSheet,
  Text,
  View
} from 'react-native';

import {
  FBSDK,
  LoginButton,
  AccessToken
} from 'react-native-fbsdk';

import { Actions } from 'react-native-router-flusk';

export default class LoginView extends React.Component {
  handleLoginFinish = (error, result) => {
      if (error) {
        alert("login has error: " + result.error);
      } else if (result.isCancelled) {
        alert("login is cancelled.");
      } else {
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            Actions.root()
          }
        )
      }
    }


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Bienvenido a PlatziMusic</Text>
        <LoginButton
          readPermissions={['public_profile', 'email']}
          onLoginFinished={this.handleLoginFinish}
          onLogoutFinished={() => alert("logout.")}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    paddingTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  }
});
