import React from 'react';

import {
  FlatList,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import ArtistBox from './artistBox.component';

export default class ArtistList extends React.Component {
  _onPressArtistItem(artist) {
    Actions.artistDetail({ artist });
  }

  render() {
    const { artists } = this.props;
    return (
      <FlatList
        data={artists}
        renderItem={
          ({item}) => {
            return (
              <TouchableOpacity
                onPress={() => this._onPressArtistItem(item)}>
                <ArtistBox artist={item} id={item.name} />
              </TouchableOpacity>
            )
          }
        } />
    );
  }
}
